package jvb;

/**
 * Created by sophiechen on 13/06/2015.
 */
public class SparseIndex2D {

    public int m, n;
    public int[] rows, cols, nnzi;

    /**
     * SparseIndex2D
     * @param n - number of rows
     * @param m - number of columns
     * @param nnzi - row-major, 1-based indices of non-zeros elements
     */
    public SparseIndex2D(int n, int m, int[] nnzi) {
        this.n = n;
        this.m = m;
        this.nnzi = nnzi;

        // convert to row/column format
        rows = new int[nnzi.length];
        cols = new int[m+1];

        int icol = 0;
        cols[icol] = 0;
        for (int i = 0; i < nnzi.length; i++) {
            nnzi[i] -= 1;
            rows[i] = nnzi[i] % n;
            if ((nnzi[i]/n) > icol) {
                icol += 1;
                cols[icol] = i;
            }
        }
        cols[cols.length - 1] = nnzi.length;
    }

    public int nbytes() {
        return (rows.length + cols.length + nnzi.length + 2) * 4;
    }

    @Override
    public String toString() {
        return "SparseIndex2D{" +
                "m=" + m +
                ", n=" + n +
                ", nnz=" + nnzi.length +
                '}';
    }
}
