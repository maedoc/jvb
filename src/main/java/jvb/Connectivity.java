package jvb;

import jvb.coupling.Coupling;

/**
 * Created by sophiechen on 13/06/2015.
 *
 * Two cases
 *
 * - coupling function defined in Java, computed inside loop
 * - % in MATLAB, prepare intermediate 'exploded' before local reduction
 *
 * In general we handle pre & post 'summation' functions.
 */

public class Connectivity {

    int iter, horizon;
    SparseIndex2D spIdx;
    float[] weights, history;
    int[] delays;
    Coupling cfun;

    /**
     * Connectivity constructor
     * @param spIdx - SparseIndex2D
     * @param weights - Non-zeros weights (row-major)
     * @param delays - Delays for non-zero weights (row-major)
     * @param cfun - Coupling
     */
    public Connectivity(SparseIndex2D spIdx, float[] weights, int[] delays, Coupling cfun) {
        iter = 0;
        this.horizon = Util.maxi(delays) + 1;
        this.spIdx = spIdx;
        this.weights = weights;
        this.delays = delays;
        this.cfun = cfun;
        history = new float[horizon * spIdx.n];
    }

    public int nbytes() {
        int sum = spIdx.nbytes() + cfun.nbytes();
        return 4 * (2 + weights.length + history.length + delays.length);
    }

    /**
     * Propagate efferent state through connectivity, advancing internal state
     * by one step in time.
     *
     * @param efferent - efferent state vector
     * @return afferent state vector
     */
    public float[] propagate(float[] efferent) {
        int hidx, n = efferent.length;
        float[] out = new float[n];

        System.arraycopy(efferent, 0, history, iter * n, n);

        for (int i=0; i<n; i++) {
            float acc = 0.0f;
            for (int j=spIdx.cols[i]; j<spIdx.cols[i+1]; j++) {
                hidx = Util.mod(iter - delays[j], horizon) * n + i;
                acc += weights[j] * cfun.pre(efferent[spIdx.rows[j]], history[hidx]);
            }
            out[i] = cfun.post(acc);
        }

        iter = Util.mod(iter + 1, horizon);

        return out;
    }

    @Override
    public String toString() {
        return "Connectivity{" +
                "cfun=" + cfun +
                ", horizon=" + horizon +
                ", iter=" + iter +
                ", spIdx=" + spIdx +
                '}';
    }
}
