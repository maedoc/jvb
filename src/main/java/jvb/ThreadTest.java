package jvb;

/**
 * Created by sophiechen on 13/06/2015.
 */
public class ThreadTest extends Thread {
    int x;

    public ThreadTest(int x) {
        this.x = x;
    }

    @Override
    public void run() {
        while (x < 100) {
            try {
                sleep(500);
            } catch (Exception e) {
                System.out.println("sleep failed: " + e.toString());
            }
            x++;
            if ((x % 5) == 0) {
                System.out.println("x = " + x);
            }
        }
    }

    @Override
    public String toString() {
        return "ThreadTest{" +
                "x=" + x +
                '}';
    }
}
