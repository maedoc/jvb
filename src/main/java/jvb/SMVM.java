package jvb;

/***
 * Sparse Matrix Vector Multiplication
 */
public class SMVM {

    int n, m;
    float[] els;
    SparseIndex2D spIdx;

    public SMVM(int n, int m, int[] nnzi, float[] els) {
        this.n = n;
        this.m = m;
        this.els = els;
        spIdx = new SparseIndex2D(n, m, nnzi);
    }

    public float[] dot(float[] x) {
        float[] out = new float[m];
        dot_slice(new Slice(m), x, out);
        return out;
    }

    public void dot_slice(Slice slice, float[] x, float[] out) {
        for (int i=slice.getStart(); i<slice.getEnd(); i+=slice.getStep()) {
            float acc = 0.0f;
            for (int j = spIdx.cols[i]; j < spIdx.cols[i+1]; j++)
                acc += els[j] * x[spIdx.rows[j]];
            out[i] = acc;
        }
    }

    public int nbytes () {
        return spIdx.nbytes() + els.length * 4;
    }

    @Override
    public String toString() {
        return "SMVM{" +
                "n=" + n +
                ", m=" + m +
                ", spIdx=" + spIdx +
                '}';
    }
}