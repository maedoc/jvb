package jvb;


public class GenOsc {

    float a, tau, d;

    public GenOsc(float a, float tau, float d) {
        this.a = a;
        this.tau = tau;
        this.d = d;
    }

    public float[] trajectory(float dt, float t, float[] xy) {
        int nSteps = Math.round(t / dt);
        float[] out = new float[nSteps];

        float x=xy[0], y=xy[1];
        for (int i=0; i<nSteps; i++) {
            x += dt * d * (x - x*x*x/3 + y) * tau;
            y += dt * d * (a - x) / tau;
            out[i] = x;
        }

        return out;
    }

    @Override
    public String toString() {
        return "GenOsc{" +
                "a=" + a +
                ", tau=" + tau +
                ", d=" + d +
                '}';
    }

}