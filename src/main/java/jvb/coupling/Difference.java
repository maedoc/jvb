package jvb.coupling;

/**
 * Created by sophiechen on 13/06/2015.
 */
public class Difference extends Linear implements Coupling {

    boolean reversed;

    public Difference(float a, float b) {
        super(a, b);
        reversed = false;
    }

    public Difference(float a, float b, boolean reversed) {
        super(a, b);
        this.reversed = reversed;
    }

    @Override
    public float pre(float x_i, float x_j) {
        if (reversed)
            return x_i - x_j;
        else
            return x_j - x_i;
    }

    @Override
    public int nbytes() {
        // TODO how big is a boolean?
        return super.nbytes() + 4;
    }

    @Override
    public String toString() {
        return "Difference{" +
                "reversed=" + reversed +
                "} " + super.toString();
    }
}
