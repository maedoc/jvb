package jvb.coupling;

/**
 * Created by sophiechen on 13/06/2015.
 */
public class PreSigmoidal extends Linear implements Coupling {

    public PreSigmoidal(float a, float b) {
        super(a, b);
    }

    @Override
    public float pre(float x_i, float x_j) {
        float ex = (float) Math.exp((double) -x_j);
        return 1.0f / (1.0f + ex);
    }

    @Override
    public String toString() {
        return "PreSigmoidal{} " + super.toString();
    }
}
