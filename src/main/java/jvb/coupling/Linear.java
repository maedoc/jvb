package jvb.coupling;

/**
 * Created by sophiechen on 13/06/2015.
 */
public class Linear implements Coupling {

    float a, b;

    public Linear(float a, float b) {
        this.a = a;
        this.b = b;
    }

    public float getA() {
        return a;
    }

    public void setA(float a) {
        this.a = a;
    }

    public float getB() {
        return b;
    }

    public void setB(float b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "Linear{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }

    @Override
    public float pre(float x_i, float x_j) {
        return x_j;
    }

    @Override
    public float post(float gx) {
        return a * gx + b;
    }

    @Override
    public int nbytes() {
        return 2 * 4;
    }
}
