package jvb.coupling;

/**
 * Created by sophiechen on 13/06/2015.
 */
public interface Coupling {

    /**
     * Computes the pre-summation coupling function
     * @param x_i - post-synaptic state
     * @param x_j - pre-synaptic state
     * @return quantity to be summed over
     */
    float pre(float x_i, float x_j);


    /**
     * Compute post-summation part of the coupling function
     * @param gx - summed state
     * @return effective coupling quantity
     */
    float post(float gx);

    int nbytes();
}
