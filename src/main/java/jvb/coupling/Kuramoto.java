package jvb.coupling;

/**
 * Created by sophiechen on 13/06/2015.
 */
public class Kuramoto extends Difference implements Coupling {

    public Kuramoto(float a, float b) {
        super(a, b);
    }

    public Kuramoto(float a, float b, boolean reversed) {
        super(a, b, reversed);
    }

    @Override
    public float pre(float x_i, float x_j) {
        return (float) Math.sin((double) super.pre(x_i, x_j));
    }

    @Override
    public String toString() {
        return "Kuramoto{} " + super.toString();
    }
}
