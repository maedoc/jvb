package jvb.scheme;

/** interface Scheme.
*/
public interface Scheme
{
   public void setDt(float dt);
   public float dt();
   public int order();
   public boolean isStochastic();
} // Scheme