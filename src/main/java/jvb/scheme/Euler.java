package jvb.scheme;

/** class Euler.
*/
public class Euler extends BaseScheme implements Scheme
{
   /** Constructor. */
   public Euler()
   {
      super();
   }

	public int order() {
	   return 1;
	}

	public boolean isStochastic() {
	   return false;
	}

	float _dt;
}