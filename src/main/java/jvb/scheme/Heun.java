package jvb.scheme;

public class Heun extends BaseScheme implements Scheme
{
   public Heun()
   {
      super();
   }

   public int order() {
      return 2;
   }

   public boolean isStochastic() {
      return false;
   }
}