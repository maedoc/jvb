package jvb.scheme;

/** class RK2S implements a Runge-Kutta 2nd order scheme for
* stochastic systems, cf
*http://en.wikipedia.org/wiki/Runge%E2%80%93Kutta_method_(SDE)
*/
public class RK2S extends BaseScheme
{
   /** Constructor. */
   public RK2S()
   {
      super();
   }

   public boolean isStochastic() {
      return true;
   }

   public int order() {
      return 2;
   }
}