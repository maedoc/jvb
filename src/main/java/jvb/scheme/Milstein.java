package jvb.scheme;

/** class Milstein, provides better error than EulerMaruyama, cf
* http://en.wikipedia.org/wiki/Milstein_method
*/

public class Milstein extends BaseScheme implements Scheme
{
   /** Constructor. */
   public Milstein()
   {
      super();
   }

	public boolean isStochastic() {
	   return true;
	}

	public int order() {
	   return 1;
	}
}