package jvb.scheme;

public class BaseScheme
{
	public BaseScheme()
	{
	this._dt = 1.0f;
	}

	public void setDt(float dt) {
	this._dt = dt;
	}

	public float dt() {
	return _dt;
	}

	private float _dt;
}