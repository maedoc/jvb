package jvb.scheme;

/** class EulerMaruyama.
*/
public class EulerMaruyama extends Euler
{
   public EulerMaruyama()
   {
		super();
   }

	@Override
	public boolean isStochastic() {
	   return true;
	}
}