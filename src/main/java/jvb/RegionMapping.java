package jvb;

public class RegionMapping {

    int max;
    int[] map, sorted;

    public RegionMapping(int[] map) {
        this.map = Util.dec(map);
        sorted = map.clone();
        java.util.Arrays.sort(sorted);
        max = sorted[sorted.length - 1];
    }

    public float[] reduce(float[] vec) throws Exception {
        float[] out = new float[max + 1];
        java.util.Arrays.fill(out, 0.0f);
        reduce_slice(new Slice(map.length), vec, out);
        return out;
    }


    public float[] explode(float[] vec) throws Exception {
        float[] out = new float[map.length];
        explode_slice(new Slice(map.length), vec, out);
        return out;
    }

    public void reduce_slice(Slice slice, float[] vec, float[] out) {
        for (int i= slice.getStart(); i<slice.getEnd(); i+=slice.getStep())
            out[map[i]] += vec[i];
    }

    public void explode_slice(Slice slice, float[] vec, float[] out) {
        for (int i= slice.getStart(); i<slice.getEnd(); i+=slice.getStep())
            out[i] = vec[map[i]];
    }
}
