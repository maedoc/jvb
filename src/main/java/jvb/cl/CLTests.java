package jvb.cl;

import com.jogamp.opencl.CLContext;
import com.jogamp.opencl.CLPlatform;

/**
 * Created by sophiechen on 14/06/2015.
 */
public class CLTests {

    public CLPlatform clPlatform;
    public CLContext clContext;

    public CLTests() {
        clPlatform = CLPlatform.getDefault();
        clContext = CLContext.create(clPlatform.getMaxFlopsDevice());
    }

    @Override
    public String toString() {
        return "CLTests{" +
                "clPlatform=" + clPlatform +
                ", clContext=" + clContext +
                '}';
    }

    public static void main (String[] args) {
        CLTests clTests = new CLTests();
        System.out.println(clTests.toString());
    }
}
