package jvb.model;

/**
 * Created by sophiechen on 13/06/2015.
 */
public interface Model {

    /**
     * @return number of state variables in model
     */
    int nSVar();

    /**
     * @return number of coupling variables in model
     */
    int nCVar();

    /**
     * @return number of parameters in model
     */
    int nPar();

    /**
     * @param i - index of coupling variable
     * @return state variable index corresponding to i
     */
    int iCVar(int i);

    /**
     * @return dispersion of noise
     */
    float noiseDispersion();

    /**
     * @return exponential decorrelation time of noise
     */
    float noiseTime();

    /**
     * @param state - current state of the model
     * @param cvar - afferent coupling variables
     * @param par - model parameter values
     * @return time derivatives of model state variables
     */
    float[] dfun(float[] state, float[] cvar, float[] par);

    /**
     * @param state - current state of model
     * @return coefficients of noise
     */
    float[] gfun(float[] state);
}
