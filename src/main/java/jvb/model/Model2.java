package jvb.model;

/** interface Model.
*/
public interface Model2
{
   /** number of state variables in this model
   */
   public int nSVars();

   /** Number of coupling variables in this model
   */
   public int nCVars();

   /** Number of parameters in this model
   */
   public int nPars();

   /** Evaluate model equations
   */
	public void evaluate(float[] state, float[] rhs, float[] pars);
} // Model