package jvb.model;


/** class Oscillator.
*/
public class Oscillator implements Model2
{
   /** Constructor. */
   public Oscillator()
   {
   }

	public int nSVars() {
	   return 2;
	}

	public int nCVars() {
	   return 1;
	}

	public int nPars() {
	   return 0;
	}

	public void evaluate(float[] state, float[] rhs, float[] pars) {
		float x=state[0], y=state[1];
		rhs[0] = x - y;
		rhs[1] = y - x;
	}
}