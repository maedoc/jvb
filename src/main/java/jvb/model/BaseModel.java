package jvb.model;

/**
 * Created by sophiechen on 13/06/2015.
 */
public class BaseModel implements Model {
    /**
     * @return number of state variables in model
     */
    @Override
    public int nSVar() {
        return 0;
    }

    /**
     * @return number of coupling variables in model
     */
    @Override
    public int nCVar() {
        return 0;
    }

    /**
     * @return number of parameters in model
     */
    @Override
    public int nPar() {
        return 0;
    }

    /**
     * @param i - index of coupling variable
     * @return state variable index corresponding to i
     */
    @Override
    public int iCVar(int i) {
        return 0;
    }

    /**
     * @return dispersion of noise
     */
    @Override
    public float noiseDispersion() {
        return 0;
    }

    /**
     * @return exponential decorrelation time of noise
     */
    @Override
    public float noiseTime() {
        return 0;
    }

    /**
     * @param state - current state of the model
     * @param cvar  - afferent coupling variables
     * @param par   - model parameter values
     * @return time derivatives of model state variables
     */
    @Override
    public float[] dfun(float[] state, float[] cvar, float[] par) {
        return new float[0];
    }

    /**
     * @param state - current state of model
     * @return coefficients of noise
     */
    @Override
    public float[] gfun(float[] state) {
        return new float[0];
    }
}
