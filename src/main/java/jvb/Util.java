package jvb;

/**
 * Created by sophiechen on 13/06/2015.
 */
public class Util {

    /**
     * Decrement all elements of integer array, such as index arrays from
     * MATLAB's find that are 1-based, to be suitable for indexing the equivalent
     * Java array.
     *
     * @param idx - arrays of integers
     * @return idx decremented, modified in place
     */
    public static int[] dec(int[] idx) {
        for (int i=0; i<idx.length; i++)
            idx[i] -= 1;
        return idx;
    }

    /**
     * Generalized modulo operator, wrapping around in both directions
     * like Python's modulo operator.
     * @param i - index to wrap between 0 and n
     * @param n - modulo
     * @return index wrapped between 0 and n - 1
     */
    public static int mod(int i, int n)
    {
        if (i>=0)
            return i % n;
        else
        if ((-i) % n == 0)
            return 0;
        else
            return n + (i % n);
    }

    /**
     * Find the maximum element of integer array
     * @param idx - array to search
     * @return maximum element of idx
     */
    public static int maxi(int[] idx) {
        int max = Integer.MIN_VALUE;
        for (int i=0; i<idx.length; i++)
            if (idx[i] > max)
                max = idx[i];
        return max;
    }
}
