%% setup
clear java
n = 1638;
A = sprandn(n, n, 0.25);
smvm = jvb.SMVM(n, n, find(A), nonzeros(A));
b = randn(n, 1)';

sum((b*A - smvm.dot(b)').^2)

%%
tic
for i=1:1000
    smvm.dot(b);
end
toc

%% 
tic
for i=1:1000
    b*A;
end
toc

%% 
clear java
go = org.jvb.GenOsc(0.9, 3.0, 1.0)

%% 
clear java
rm = org.jvb.RegionMapping(int32(randi(10, 1000, 1)));
rm.reduce(rm.explode(1:10))

%%
spidx = jvb.SparseIndex2D(n, n, find(A));

%%
clear java
cf = jvb.coupling.PreSigmoidal(1.0, 0.0)
x = -10:0.1:10;
y = []
for i=1:length(x), y(i) = cf.pre(0.0, x(i)); end
plot(x, y)

%% 
clear java
cf = jvb.coupling.Linear(1.0, 0.0);
n = 16384;
A = sprandn(n, n, 0.05);
spidx = jvb.SparseIndex2D(n, n, find(A));
weights = nonzeros(A);
delays = randi(50, size(weights));
conn = jvb.Connectivity(spidx, weights, delays, cf);

%%
tic
for i=1:100
    conn.propagate(1:n);
end
toc
