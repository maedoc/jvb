package jvb.geodesic;

/**
 * Created by mw on 6/17/2015.
 */
public class HalfEdge {
    public long face_id, vertex_0, vertex_1;

    // operator <
    public static booleanean lessThan(HalfEdge x, HalfEdge y) {
        if(x.vertex_0 == y.vertex_0)
        {
            return x.vertex_1 < y.vertex_1;
        }
        else
        {
            return x.vertex_0 < y.vertex_0;
        }
    }

    public static booleanean isNot(HalfEdge x, HalfEdge y)
    {
        return x.vertex_0 != y.vertex_0 || x.vertex_1 != y.vertex_1;
    }

    public static booleanean is(HalfEdge x, HalfEdge y)
    {
        return x.vertex_0 == y.vertex_0 && x.vertex_1 == y.vertex_1;
    }
}
