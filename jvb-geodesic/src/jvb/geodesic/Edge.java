package jvb.geodesic;

/**
 * Created by mw on 6/17/2015.
 */
public class Edge extends MeshElementBase {

    public Edge()
    {
        m_type = PointType.EDGE;
    }

    double length(){return m_length;};

    face_pointer opposite_face(face_pointer f)
    {
        if(adjacent_faces().size() == 1)
        {
            assert(adjacent_faces()[0]->id() == f->id());
            return NULL;
        }

        assert(adjacent_faces()[0]->id() == f->id() ||
            adjacent_faces()[1]->id() == f->id());

        return adjacent_faces()[0]->id() == f->id() ?
            adjacent_faces()[1] : adjacent_faces()[0];
    };

    vertex_pointer opposite_vertex(vertex_pointer v)
    {
        assert(belongs(v));

        return adjacent_vertices()[0]->id() == v->id() ?
            adjacent_vertices()[1] : adjacent_vertices()[0];
    };

    boolean belongs(vertex_pointer v)
    {
        return adjacent_vertices()[0]->id() == v->id() ||
            adjacent_vertices()[1]->id() == v->id();
    }

    boolean is_boundary(){return adjacent_faces().size() == 1;};

    vertex_pointer v0(){return adjacent_vertices()[0];};
    vertex_pointer v1(){return adjacent_vertices()[1];};

    void local_coordinates(Point3D* point,
                           double x,
                           double y)
    {
        double d0 = point->distance(v0());
        if(d0 < 1e-50)
        {
            x = 0.0;
            y = 0.0;
            return;
        }

        double d1 = point->distance(v1());
        if(d1 < 1e-50)
        {
            x = m_length;
            y = 0.0;
            return;
        }

        x = m_length/2.0 + (d0*d0 - d1*d1)/(2.0*m_length);
        y = sqrt(std::max(0.0, d0*d0 - x*x));
        return;
    }

    private:
    double m_length;    //length of the edge
}
