package jvb.geodesic;

/**
 * Created by mw on 6/17/2015.
 */
public class Interval {

    public Interval() {

    }

    public enum DirectionType
    {
        FROM_FACE_0,
        FROM_FACE_1,
        FROM_SOURCE,
        UNDEFINED_DIRECTION
    };

    public double signal(double x)    //geodesic distance function at point x
    {
        assert(x>=0.0 && x <= m_edge->length());

        if(m_d == GEODESIC_INF)
        {
            return GEODESIC_INF;
        }
        else
        {
            double dx = x - m_pseudo_x;
            if(m_pseudo_y == 0.0)
            {
                return m_d + std::abs(dx);
            }
            else
            {
                return m_d + sqrt(dx*dx + m_pseudo_y*m_pseudo_y);
            }
        }
    }

    double max_distance(double end)
    {
        if(m_d == GEODESIC_INF)
        {
            return GEODESIC_INF;
        }
        else
        {
            double a = std::abs(m_start - m_pseudo_x);
            double b = std::abs(end - m_pseudo_x);

            return a > b ? m_d + sqrt(a*a + m_pseudo_y*m_pseudo_y):
                    m_d + sqrt(b*b + m_pseudo_y*m_pseudo_y);
        }
    }

    void compute_min_distance(double stop)    //compute min, given c,d theta, start, end.
    {
        assert(stop > m_start);

        if(m_d == GEODESIC_INF)
        {
            m_min = GEODESIC_INF;
        }
        else if(m_start > m_pseudo_x)
        {
            m_min = signal(m_start);
        }
        else if(stop < m_pseudo_x)
        {
            m_min = signal(stop);
        }
        else
        {
            assert(m_pseudo_y<=0);
            m_min = m_d - m_pseudo_y;
        }
    }
    //compare two intervals in the queue
    boolean operator()(interval_pointer const x, interval_pointer const y) const
    {
        if(x->min() != y->min())
        {
            return x->min() < y->min();
        }
        else if(x->start() != y->start())
        {
            return x->start() < y->start();
        }
        else
        {
            return x->edge()->id() < y->edge()->id();
        }
    }

    double stop()    //return the endpoint of the interval
    {
        return m_next ? m_next->start() : m_edge->length();
    }

    double hypotenuse(double a, double b)
    {
        return sqrt(a*a + b*b);
    }

    void find_closest_point(double const x,
                            double const y,
                            double offset,
                            double distance)    //find the point on the interval that is closest to the point (alpha, s)
    {
        if(m_d == GEODESIC_INF)
        {
            r = GEODESIC_INF;
            d_out = GEODESIC_INF;
            return;
        }

        double hc = -m_pseudo_y;
        double rc = m_pseudo_x;
        double end = stop();

        double local_epsilon = SMALLEST_INTERVAL_RATIO*m_edge->length();
        if(std::abs(hs+hc) < local_epsilon)
        {
            if(rs<=m_start)
            {
                r = m_start;
                d_out = signal(m_start) + std::abs(rs - m_start);
            }
            else if(rs>=end)
            {
                r = end;
                d_out = signal(end) + fabs(end - rs);
            }
            else
            {
                r = rs;
                d_out = signal(rs);
            }
        }
        else
        {
            double ri = (rs*hc + hs*rc)/(hs+hc);

            if(ri<m_start)
            {
                r = m_start;
                d_out = signal(m_start) + hypotenuse(m_start - rs, hs);
            }
            else if(ri>end)
            {
                r = end;
                d_out = signal(end) + hypotenuse(end - rs, hs);
            }
            else
            {
                r = ri;
                d_out = m_d + hypotenuse(rc - rs, hc + hs);
            }
        }
    }

    double start(){return m_start;};
    double d(){return m_d;};
    double pseudo_x(){return m_pseudo_x;};
    double pseudo_y(){return m_pseudo_y;};
    double min(){return m_min;};
    Interval[] next(){return m_next;};
    edge_pointer& edge(){return m_edge;};
    DirectionType& direction(){return m_direction;};
    boolean visible_from_source(){return m_direction == FROM_SOURCE;};
    long& source_index(){return m_source_index;};

    void initialize(edge_pointer edge,
                    SurfacePoint* point = NULL,
                    long source_index = 0)
    {
        m_next = NULL;
        //m_geodesic_previous = NULL;
        m_direction = UNDEFINED_DIRECTION;
        m_edge = edge;
        m_source_index = source_index;

        m_start = 0.0;
        //m_stop = edge->length();
        if(!source)
        {
            m_d = GEODESIC_INF;
            m_min = GEODESIC_INF;
            return;
        }
        m_d = 0;

        if(source->base_element()->type() == VERTEX)
        {
            if(source->base_element()->id() == edge->v0()->id())
            {
                m_pseudo_x = 0.0;
                m_pseudo_y = 0.0;
                m_min = 0.0;
                return;
            }
            else if(source->base_element()->id() == edge->v1()->id())
            {
                m_pseudo_x = stop();
                m_pseudo_y = 0.0;
                m_min = 0.0;
                return;
            }
        }

        edge->local_coordinates(source, m_pseudo_x, m_pseudo_y);
        m_pseudo_y = -m_pseudo_y;

        compute_min_distance(stop());
    }

    protected:
    double m_start;               //initial point of the interval on the edge
    double m_d;                   //distance from the source to the pseudo-source
    double m_pseudo_x;            //coordinates of the pseudo-source in the local coordinate system
    double m_pseudo_y;            //y-coordinate should be always negative
    double m_min;                 //minimum distance on the interval

    interval_pointer m_next;      //pointer to the next interval in the list
    edge_pointer m_edge;          //edge that the interval belongs to
    long m_source_index;      //the source it belongs to
    DirectionType m_direction;    //where the interval is coming from
}
