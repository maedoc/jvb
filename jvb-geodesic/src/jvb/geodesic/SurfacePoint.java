package jvb.geodesic;

/**
 * Created by mw on 6/17/2015.
 */
public class SurfacePoint extends Point3D {
    public:
    SurfacePoint():
    m_p(NULL)
    {};

    SurfacePoint(vertex_pointer v):    //set the surface point in the vertex
    SurfacePoint::Point3D(v),
    m_p(v)
    {};

    SurfacePoint(face_pointer f):    //set the surface point in the center of the face
    m_p(f)
    {
        set(0,0,0);
        add(f->adjacent_vertices()[0]);
        add(f->adjacent_vertices()[1]);
        add(f->adjacent_vertices()[2]);
        multiply(1./3.);
    };

    SurfacePoint(edge_pointer e,    //set the surface point in the middle of the edge
                 double a = 0.5):
    m_p(e)
    {
        double b = 1 - a;

        vertex_pointer v0 = e->adjacent_vertices()[0];
        vertex_pointer v1 = e->adjacent_vertices()[1];

        x() = b*v0->x() + a*v1->x();
        y() = b*v0->y() + a*v1->y();
        z() = b*v0->z() + a*v1->z();
    };

    SurfacePoint(base_pointer g,
                 double x,
                 double y,
                 double z,
                 PointType t = UNDEFINED_POINT):
    m_p(g)
    {
        set(x,y,z);
    };

    void initialize(SurfacePoint const& p)
    {
        *this = p;
    }

    ~SurfacePoint(){};

    PointType type(){return m_p ? m_p->type() : UNDEFINED_POINT;};
    base_pointer& base_element(){return m_p;};
    protected:
    base_pointer m_p;    //could be face, vertex or edge pointer

}
