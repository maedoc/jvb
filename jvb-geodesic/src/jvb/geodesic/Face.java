package jvb.geodesic;

/**
 * Created by mw on 6/17/2015.
 */
public class Face extends MeshElementBase {

    public Face()
    {
        m_type = PointType.FACE;
    }

    edge_pointer opposite_edge(vertex_pointer v) {
        for(long i=0; i<3; ++i)
        {
            edge_pointer e = adjacent_edges()[i];
            if(!e->belongs(v))
            {
                return e;
            }
        }
        assert(0);
        return NULL;
    }

    vertex_pointer opposite_vertex(edge_pointer e) {
        for(long i=0; i<3; ++i)
        {
            vertex_pointer v = adjacent_vertices()[i];
            if(!e->belongs(v))
            {
                return v;
            }
        }
        assert(0);
        return NULL;
    }

    edge_pointer next_edge(edge_pointer e, vertex_pointer v) {
        assert(e->belongs(v));

        for(long i=0; i<3; ++i)
        {
            edge_pointer next = adjacent_edges()[i];
            if(e->id() != next->id() && next->belongs(v))
            {
                return next;
            }
        }
        assert(0);
        return NULL;
    }

    double vertex_angle(vertex_pointer v)
    {
        for(long i=0; i<3; ++i)
        {
            if(adjacent_vertices()[i]->id() == v->id())
            {
                return m_corner_angles[i];
            }
        }
        assert(0);
        return 0;
    }

    double* corner_angles(){return m_corner_angles;};

    private:
    /*
    Triangle angles in radians; angles correspond to vertices in
    m_adjacent_vertices
    */
    double m_corner_angles[3];

}
