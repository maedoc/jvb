package jvb.geodesic;

/**
 * Created by mw on 6/17/2015.
 */
public class IntervalWithStop extends Interval {

    public double stop() {
        return m_stop;
    }

    protected double m_stop;

}
