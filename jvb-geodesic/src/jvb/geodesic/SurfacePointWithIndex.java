package jvb.geodesic;

/**
 * Created by mw on 6/17/2015.
 */
public class SurfacePointWithIndex extends SurfacePoint {
    public:
    long index(){return m_index;};

    void initialize(SurfacePoint& p, long index)
    {
        SurfacePoint::initialize(p);
        m_index = index;
    }

    boolean operator()(SurfacePointWithIndex* x, SurfacePointWithIndex* y) const //used for sorting
    {
        assert(x->type() != UNDEFINED_POINT && y->type() !=UNDEFINED_POINT);

        if(x->type() != y->type())
        {
            return x->type() < y->type();
        }
        else
        {
            return x->base_element()->id() < y->base_element()->id();
        }
    }

    private:
    long m_index;
}
