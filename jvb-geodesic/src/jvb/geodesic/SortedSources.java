package jvb.geodesic;

/**
 * Created by mw on 6/17/2015.
 */

// class SortedSources : public std::vector<SurfacePointWithIndex>
public class SortedSources {
    private:
    typedef std::vector<SurfacePointWithIndex*> sorted_vector_type;
    public:
    typedef sorted_vector_type::iterator sorted_iterator;
    typedef std::pair<sorted_iterator, sorted_iterator> sorted_iterator_pair;

    sorted_iterator_pair sources(base_pointer mesh_element)
    {
        m_search_dummy.base_element() = mesh_element;

        return equal_range(m_sorted.begin(),
                m_sorted.end(),
                &m_search_dummy,
        m_compare_less);
    }

    void initialize(std::vector<SurfacePoint>& sources)    //we initialize the sources by copie
    {
        resize(sources.size());
        m_sorted.resize(sources.size());
        for(long i=0; i<sources.size(); ++i)
        {
            SurfacePointWithIndex& p = *(begin() + i);

            p.initialize(sources[i],i);
            m_sorted[i] = &p;
        }

        std::sort(m_sorted.begin(), m_sorted.end(), m_compare_less);
    };

    SurfacePointWithIndex& operator[](long i)
    {
        assert(i < size());
        return *(begin() + i);
    }

    private:
    sorted_vector_type m_sorted;
    SurfacePointWithIndex m_search_dummy;    //used as a search template
    SurfacePointWithIndex m_compare_less;    //used as a compare functor

}
