package jvb.geodesic;

/**
 * Created by mw on 6/17/2015.
 */
public class IntervalList {

    IntervalList(){m_first = NULL;};

    void clear()
    {
        m_first = NULL;
    };

    void initialize(edge_pointer e)
    {
        m_edge = e;
        m_first = NULL;
    };

    interval_pointer covering_interval(double offset)    //returns the interval that covers the offset
    {
        assert(offset >= 0.0 && offset <= m_edge->length());

        interval_pointer p = m_first;
        while(p && p->stop() < offset)
        {
            p = p->next();
        }

        return p;// && p->start() <= offset ? p : NULL;
    };

    void find_closest_point(SurfacePoint* point,
                            double offset,
                            double distance,
                            interval_pointer& interval)
    {
        interval_pointer p = m_first;
        distance = GEODESIC_INF;
        interval = NULL;

        double x,y;
        m_edge->local_coordinates(point, x, y);

        while(p)
        {
            if(p->min()<GEODESIC_INF)
            {
                double o, d;
                p->find_closest_point(x, y, o, d);
                if(d < distance)
                {
                    distance = d;
                    offset = o;
                    interval = p;
                }
            }
            p = p->next();
        }
    };

    long number_of_intervals()
    {
        interval_pointer p = m_first;
        long count = 0;
        while(p)
        {
            ++count;
            p = p->next();
        }
        return count;
    }

    interval_pointer last()
    {
        interval_pointer p = m_first;
        if(p)
        {
            while(p->next())
            {
                p = p->next();
            }
        }
        return p;
    }

    double signal(double x)
    {
        interval_pointer interval = covering_interval(x);

        return interval ? interval->signal(x) : GEODESIC_INF;
    }

    interval_pointer& first(){return m_first;};
    edge_pointer& edge(){return m_edge;};

    private interval_pointer m_first;    //pointer to the first member of the list
    private edge_pointer m_edge;         //edge that owns this list
}
