package jvb.geodesic;


/**
 * Created by mw on 6/17/2015.
 */
public class Point3D extends MeshElementBase {
    public Point3D(){};

    public Point3D(Point3D p)
    {
        x() = p->x();
        y() = p->y();
        z() = p->z();
    };

    double[] xyz(){return m_coordinates;}
    double x(){return m_coordinates[0];}
    double y(){return m_coordinates[1];}
    double z(){return m_coordinates[2];}

    void set(double new_x, double new_y, double new_z)
    {
        x() = new_x;
        y() = new_y;
        z() = new_z;
    }

    void set(double* data)
    {
        x() = *data;
        y() = *(data+1);
        z() = *(data+2);
    }

    double distance(double* v)
    {
        double dx = m_coordinates[0] - v[0];
        double dy = m_coordinates[1] - v[1];
        double dz = m_coordinates[2] - v[2];

        return sqrt(dx*dx + dy*dy + dz*dz);
    };

    double distance(Point3D* v)
    {
        return distance(v->xyz());
    };

    void add(Point3D* v)
    {
        x() += v->x();
        y() += v->y();
        z() += v->z();
    };

    void multiply(double v)
    {
        x() *= v;
        y() *= v;
        z() *= v;
    };

    private double m_coordinates[];    //xyz
}
