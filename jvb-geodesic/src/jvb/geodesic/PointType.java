package jvb.geodesic;

/**
 * Created by mw on 6/17/2015.
 */
public enum PointType {
    VERTEX,
    EDGE,
    FACE,
    UNDEFINED_POINT
}
