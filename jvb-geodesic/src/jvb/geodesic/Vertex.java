package jvb.geodesic;

/**
 * Created by mw on 6/17/2015.
 */
public class Vertex extends Point3D {

    public Vertex()
    {
        m_type = PointType.VERTEX;
    }

    public booleanean saddle_or_boundary(){return m_saddle_or_boundary;};

    /*
    This flag speeds up exact geodesic algorithm, it is true if total adjacent
    angle is larger than 2*PI or this vertex belongs to the mesh boundary.
    */
    private booleanean m_saddle_or_boundary;

}
