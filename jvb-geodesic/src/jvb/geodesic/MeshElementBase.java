package jvb.geodesic;

/**
 * Created by mw on 6/17/2015.
 */
public class MeshElementBase {
    public:
    /*
    typedef SimpleVector<vertex_pointer> vertex_pointer_vector;
    typedef SimpleVector<edge_pointer> edge_pointer_vector;
    typedef SimpleVector<face_pointer> face_pointer_vector;
    */

    MeshElementBase():
    m_id(0),
    m_type(UNDEFINED_POINT)
    {};

    vertex_pointer_vector& adjacent_vertices(){return m_adjacent_vertices;};
    edge_pointer_vector& adjacent_edges(){return m_adjacent_edges;};
    face_pointer_vector& adjacent_faces(){return m_adjacent_faces;};

    long& id(){return m_id;};
    PointType type(){return m_type;};

    protected:
    vertex_pointer_vector m_adjacent_vertices;  //list of the adjacent vertices
    edge_pointer_vector m_adjacent_edges;       //list of the adjacent edges
    face_pointer_vector m_adjacent_faces;       //list of the adjacent faces

    long m_id;     //unique id
    PointType m_type;  //vertex, edge or face
}
