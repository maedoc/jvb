package jvb.geodesic;

/**
 * Created by mw on 6/17/2015.
 *
 * Namespace for geodesic_constants_and_simple_functions.h and any
 * other global stuff.
 *
 * NB: Some of the constants are global parameters of the method.
 *
 */

public class Geodesic {

    public static double M_PI, GEODESIC_INF, SMALLEST_INTERVAL_RATIO;

    static {
        M_PI = 3.14159265358979323846;
        GEODESIC_INF = 1e100;
        SMALLEST_INTERVAL_RATIO = 1e-6;
    }

    //compute the cosine of the angle given the lengths of the edges
    public static double cos_from_edges(
            double a, double b, double c)
    {
        // TODO clean this up
        assert(a>1e-50);
        assert(b>1e-50);
        assert(c>1e-50);

        double result = (b*b + c*c - a*a)/(2.0*b*c);
        result = Math.max(result, -1.0);
        return Math.min(result, 1.0);
    }

    //compute the cosine of the angle given the lengths of the edges
    double angle_from_edges(double a, double b, double c)
    {
        return Math.acos(cos_from_edges(a, b, c));
    }

    template<class Points, class Faces>
    inline boolean read_mesh_from_file(char* filename,
                                    Points& points,
                                    Faces& faces)
    {
        std::ifstream file(filename);
        assert(file.is_open());
        if(!file.is_open()) return false;

        long num_points;
        file >> num_points;
        assert(num_points>=3);

        long num_faces;
        file >> num_faces;

        points.resize(num_points*3);
        for(typename Points::iterator i=points.begin(); i!=points.end(); ++i)
        {
            file >> *i;
        }

        faces.resize(num_faces*3);
        for(typename Faces::iterator i=faces.begin(); i!=faces.end(); ++i)
        {
            file >> *i;
        }
        file.close();

        return true;
    }

    public static void fill_surface_point_structure(geodesic::SurfacePoint* point,
                                                    double* data,
                                                    Mesh* mesh)
    {
        point->set(data);
        long type = (long) data[3];
        long id = (long) data[4];


        if(type == 0)         //vertex
        {
            point->base_element() = &mesh->vertices()[id];
        }
        else if(type == 1)    //edge
        {
            point->base_element() = &mesh->edges()[id];
        }
        else                 //face
        {
            point->base_element() = &mesh->faces()[id];
        }
    }

    public static void fill_surface_point_double(geodesic::SurfacePoint* point,
                                                 double* data,
                                                 long mesh_id)
    {
        data[0] = point->x();
        data[1] = point->y();
        data[2] = point->z();
        data[4] = point->base_element()->id();

        if(point->type() == VERTEX)       //vertex
        {
            data[3] = 0;
        }
        else if(point->type() == EDGE)    //edge
        {
            data[3] = 1;
        }
        else                              //face
        {
            data[3] = 2;
        }
    }

    public static double length(std::vector<SurfacePoint>& path)
    {
        double length = 0;
        if(!path.empty())
        {
            for(unsigned i=0; i<path.size()-1; ++i)
            {
                length += path[i].distance(&path[i+1]);
            }
        }
        return length;
    }

    public static void print_info_about_path(std::vector<SurfacePoint>& path)
    {
        std::cout << "number of the points in the path = " << path.size()
                << ", length of the path = " << length(path)
                << std::endl;
    }
}
